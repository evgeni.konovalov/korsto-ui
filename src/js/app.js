;-function(window, document, $, undefined) {

    $(function() {

        // для работы древовидных списков
        $('.yy-tree').on('click', '.yy-tree__item--has-child .yy-tree__item__icon, .yy-tree__item--has-child a', function(e){
            var $collapsible = $(this).closest('.yy-tree__item--has-child');
            $collapsible.parent().children('ul').slideToggle('fast', function(){
                $collapsible.toggleClass('yy-tree__item--has-child--opened');
            });
        });

        // tooltip'ы
        $(document).on('mouseenter', '.yy-tooltip__over-area', function(e){
            $(this).nextAll('.yy-tooltip__body').stop().fadeIn('fast');
        });
        $(document).on('mouseleave', '.yy-tooltip__over-area', function(e){
            $(this).nextAll('.yy-tooltip__body').stop().fadeOut('fast');
        });

        // фильтр товаров
        $(document).on('click', '.yy-product-list__filter-switch', function(){
            $(this).toggleClass('yy-product-list__filter-switch--opened');
            $('.yy-product-list__filter').toggleClass('yy-product-list__filter--opened');
            $('.yy-product--js-resizeable').toggleClass('yy-product--m-size');
        });

        // селектбоксы
        $('.yy-input-select>select').niceSelect();

        // слайдер
        slider = $('.yy-input-range__slider').nstSlider({
            "crossable_handles": false,
            "left_grip_selector": ".yy-input-range__left-grip",
            "right_grip_selector": ".yy-input-range__right-grip",
            "value_bar_selector": ".yy-input-range__bar",
            "value_changed_callback": function(cause, leftValue, rightValue) {
                $(this).closest('.yy-input-range').find('.yy-input-range__left-label').val(leftValue);
                $(this).closest('.yy-input-range').find('.yy-input-range__right-label').val(rightValue);
            }
        });
        $(document).on('change', '.yy-input-range__label', function(e){
            var left  = $(this).parent().find('.yy-input-range__left-label').val();
            var right = $(this).parent().find('.yy-input-range__right-label').val();
            $(this).closest('.yy-input-range').find('.nstSlider').nstSlider('set_position', left, right);
        });

        // placeholders
        $(document).on('change', '.yy-input-text', function(e){
            if (!!$(this).val()) {
                $(this).siblings('.yy-input-placeholder').addClass('yy-input-placeholder--hidden');
            } else {
                $(this).siblings('.yy-input-placeholder').removeClass('yy-input-placeholder--hidden');
            }
        });

        // ресайз заголовков моделей
        $('.yy-model__name').textfill({
            minFontPixels: 8,
            maxFontPixels: 18,
            changeLineHeight: true,
            fail: function() {
                console.log("textfill.js error!");
            },
            complete: function() {
                $('.yy-model__name').animate({'opacity': 1});
            }
        });
        $('.yy-model__data').textfill({
            minFontPixels: 8,
            maxFontPixels: 14,
            changeLineHeight: true,
            fail: function() {
                console.log("textfill.js error!");
            },
            complete: function() {
                $('.yy-model__data').animate({'opacity': 1});
            }
        });
        $('.yy-product__name').textfill({
            minFontPixels: 8,
            maxFontPixels: 14,
            changeLineHeight: true,
            innerTag: 'a',
            fail: function() {
                console.log("textfill.js error!");
            },
            complete: function() {
                $('.yy-product__name').animate({'opacity': 1});
            }
        });
    });

}(window, document, jQuery);